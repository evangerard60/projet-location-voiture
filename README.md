# Projet location voiture

# Documentation

C'est le projet de moi même Evan pour une location de voiture pour les nuls qui apprennent à conduire.

Mon segment d'utilisateurs est basé sur les personnes qui n'ont pas de véhicules.

Ma problématique est que les gens n'ont pas de véhicule.

Ma proposition de valeure est que je veux faire louer pleins de véhicules de pleins de types différents.

Mais contrairement aux autres, mon site proposera des voitures pour les apprentis-conducteurs qui eux ne possèdent pas forcément de véhicules pour apprendre.

Mon critère de succès est dans le fait que j'aurai tout type de véhicule et je toucherai tous les marchés des automobiles.

Il y en aura sur les camions, voitures, camionettes, motos etc...

# MVP

Pour pouvoir mettre en ligne mon site avec le strict minimum, il me faut une base de données qui regroupe tous les véhicules disponibles avec la description, leur type et une photo. 

Un système pour louer un véhicule : nombre killomètres, quand, cmb de temps etc... 

Un site qui regroupe tout ça.

# Communication

- Pour afficher la page principale 
  - GET
  - index.php
  - (optionnel) date
- Pour afficher la voiture
  - GET
  - voiture.php
  - id:voiture
- Pour louer une voiture
  - POST
  - id_voiture
  - id_client
  - date_debut
  - date_fin
  - nb_kil_par_mois
  - prix final
- Pour annuler une location
  - DELETE
  - id_voiture
  - id_client
  - date_debut
  - date_fin
  - nb_kil_par_mois
  - prix final

